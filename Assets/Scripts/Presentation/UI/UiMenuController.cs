using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiMenuController : MonoBehaviour
{
    private GameObject Canvas_GameOver;
    private GameObject Canvas_Win;
    private GameObject Canvas_Pause;

    private static UiMenuController instance = null;

    public List<Image> GameObjects;

    // Game Instance Singleton
    public static UiMenuController Instance
    {
        get
        {
            return instance;
        }
    }

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        instance = this;

        Canvas_GameOver = GameObject.Find("Canvas_GameOver");
        Canvas_GameOver?.SetActive(false);

        Canvas_Win = GameObject.Find("Canvas_Win");
        Canvas_Win?.SetActive(false);

        Canvas_Pause = GameObject.Find("Canvas_Pause");
        Canvas_Pause?.SetActive(false);
    }

    public void ResetLevel()
    {
        var scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
        Time.timeScale = 1;
    }

    public void OpenLevel(string name)
    {
        SceneManager.LoadScene(name);
        Time.timeScale = 1;
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ShowGameOver()
    {
        Canvas_GameOver.SetActive(true);
        Time.timeScale = 0;
    }

    public void ShowGameWin()
    {
        Canvas_Win.SetActive(true);
        Time.timeScale = 0;
    }

    public void ShowPause()
    {
        Canvas_Pause.SetActive(true);
        Time.timeScale = 0;
    }

    public void Resume()
    {
        Canvas_Pause.SetActive(false);
        Time.timeScale = 1;
    }

    public void CheckItem(string name)
    {
        var image = GameObjects.Find(p => p.name == name);
        image.color = Color.white;
    }
}