using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScrollingText : MonoBehaviour
{
    public TextMeshProUGUI text;
    private RectTransform rectTransform;
    public float speed = .03F;

    private float time;

    private Vector3 _originalPosition;

    private void Awake()
    {
        rectTransform = text.GetComponent<RectTransform>();
        _originalPosition = new Vector3(rectTransform.position.x, rectTransform.position.y, rectTransform.position.z);
    }

    private void Update()
    {
        rectTransform.position = new Vector3(rectTransform.position.x, rectTransform.position.y + speed, rectTransform.position.z);
        time += Time.deltaTime;
        if (time > 20)
        {
            rectTransform.position = new Vector4(_originalPosition.x, _originalPosition.y, _originalPosition.z);
            time = 0;
        }
    }
}