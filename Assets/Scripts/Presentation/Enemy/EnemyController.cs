using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private Vector2 _initialPosition;
    private Vector2 _target;
    [SerializeField] private int _movimentRange = 1;
    [SerializeField] private int _speed = 5;
    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    private void Start()
    {
        _initialPosition = new Vector2(transform.position.x, transform.position.y);
        _target = new Vector2(_initialPosition.x + _movimentRange, transform.position.y);
    }

    // Update is called once per frame
    private void Update()
    {
        float step = _speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, _target, step);

        _spriteRenderer.flipX = _target.x > _initialPosition.x;

        if (Vector3.Distance(transform.position, _target) < 0.001f)
        {
            if (_target.x > _initialPosition.x)
                _target = new Vector2(_initialPosition.x - _movimentRange, transform.position.y);
            else
                _target = new Vector2(_initialPosition.x + _movimentRange, transform.position.y);
        }
    }
}