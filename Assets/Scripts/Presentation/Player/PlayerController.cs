using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, IPlayerMovimentController
{
    [SerializeField] private float _speed = 4.0f;
    [SerializeField] private float _jumpForce = 4.0f;
    [SerializeField] private float _magnetismForce = 100.0f;
    [SerializeField] private LayerMask _layerMask;

    private int objectosRecolectados;

    private Rigidbody2D _playerRb;
    private Animator _animRobot;
    private Animator _animHumano;

    private IPlayerMovimentService _movimentService;
    private bool _isGrounded;
    private Vector2 _lastMovDirection;
    private SpriteRenderer _spriteRendererRobot;
    private SpriteRenderer _spriteRendererHumano;
    private Transform _transformHumano;

    private void Awake()
    {
        _playerRb = GetComponent<Rigidbody2D>();
        _animRobot = this.transform.GetChild(0).GetComponentInChildren<Animator>();
        _animHumano = this.transform.GetChild(1).GetComponentInChildren<Animator>();
        _spriteRendererRobot = this.transform.GetChild(0).GetComponent<SpriteRenderer>();
        _spriteRendererHumano = this.transform.GetChild(1).GetComponent<SpriteRenderer>();
        _transformHumano = this.transform.GetChild(1).GetComponent<Transform>();
    }

    public void Configuration(IPlayerMovimentService playerMovimentService)
    {
        _movimentService = playerMovimentService;
    }

    private void Update()
    {
        if (Time.timeScale == 0) return; //TODO: Mejorar pausa
        if (Input.GetKey(KeyCode.Escape))
            UiMenuController.Instance.ShowPause();

        UpdateMoviment();

        UpdateMagnetism();

        UpdateAttack();
    }

    private void UpdateMagnetism()
    {
        MagnetismAction action;
        if (Input.GetKey(KeyCode.E))
            action = MagnetismAction.Atract;
        else if (Input.GetKey(KeyCode.Q))
            action = MagnetismAction.Repel;
        else
            action = MagnetismAction.None;
        UseMagnetism(action);
    }

    private void UpdateMoviment()
    {
        var lastMovement = new Vector2(Input.GetAxisRaw(AxisLabelConstants.HorizontalLabel), _playerRb.velocity.y);
        _isGrounded = _playerRb.velocity.y == 0;

        if (Input.GetKeyDown(KeyCode.Space) && _isGrounded)
        {
            lastMovement.y = _jumpForce;
        }
        if (lastMovement.x != 0)
        {
            _lastMovDirection = new Vector2(lastMovement.x, 0);
            _transformHumano.position = new Vector3(transform.position.x + (_lastMovDirection.x >= 0 ? -.5f : .5f), _transformHumano.position.y, _transformHumano.position.z);
        }
        _movimentService.Move(lastMovement, _speed);

        _spriteRendererRobot.flipX = _lastMovDirection.x < 0;
        _spriteRendererHumano.flipX = _lastMovDirection.x < 0;

        _animRobot.SetBool("IsRunning", lastMovement.x != 0);
    }

    private void UpdateAttack()
    {
        if (Input.GetMouseButtonDown(0))
        {
            var bullet = ObjectPool.Instance
                                    .Spawn(PoolObjectConstants.BULLET_POOL_TAG, transform.position, transform.rotation)
                                    .GetComponent<BulletController>();
            if (_lastMovDirection.x == 0)
            {
                _lastMovDirection.x = 1;
            }
            bullet.Direction = _lastMovDirection;
            _animHumano.SetTrigger("Attack");
        }
    }

    private void UseMagnetism(MagnetismAction action)
    {
        var magneticObjects = BoxCastDrawer.BoxCastAndDraw(new Vector2(transform.position.x, transform.position.y - 1.5f), Vector2.one, 0, _lastMovDirection, _magnetismForce, _layerMask);

        if (!magneticObjects) return;
        var logic = magneticObjects.transform.GetComponent<MagneticObjectController>();

        switch (action)
        {
            case MagnetismAction.Atract:
                logic.MagnetismOn(magneticObjects.transform.position, transform.position);
                break;

            case MagnetismAction.Repel:
                logic.MagnetismOn(transform.position, magneticObjects.transform.position);
                break;

            case MagnetismAction.None:
                logic.MagnetismOff();
                break;

            default:
                break;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            UiMenuController.Instance.ShowGameOver();
        }

        if (collision.gameObject.tag == "Items")
        {
            UiMenuController.Instance.CheckItem(collision.gameObject.name);
            Destroy(collision.gameObject);
            objectosRecolectados++;
            if (objectosRecolectados == 5)
            {
                UiMenuController.Instance.ShowGameWin();
            }
        }
    }

    void IPlayerMovimentController.Move(Vector2 direction)
    {
        _playerRb.velocity = direction;
    }
}