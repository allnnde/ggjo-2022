using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour, IPoolable
{
    [SerializeField]
    private float _speed = 5f;

    public Vector2 Direction = Vector2.right;

    [SerializeField]
    private float _lifeMaxSeg = 5;

    private float _lifeTime;

    public GameObject Owner => gameObject;

    // Update is called once per frame
    private void Update()
    {
        transform.Translate(Direction * _speed * Time.deltaTime);
        _lifeTime += Time.deltaTime;
        if (_lifeTime > _lifeMaxSeg)
        {
            ObjectPool.Instance.Despawn(PoolObjectConstants.BULLET_POOL_TAG, this.gameObject);
        }
    }

    void IPoolable.OnInstanciate(Transform parent)
    {
        transform.parent = parent;
        gameObject.SetActive(false);
    }

    public void OnSpawn(Vector3 position, Quaternion rotation)
    {
        _lifeTime = 0;
        transform.position = position;
        transform.rotation = rotation;
        gameObject.SetActive(true);
    }

    public void OnDespawn()
    {
        gameObject.SetActive(false);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name);
        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(collision.gameObject);
            OnDespawn();
        }
    }
}