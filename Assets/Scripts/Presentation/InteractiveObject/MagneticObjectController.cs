using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticObjectController : MonoBehaviour, IMagneticObjectController
{
    private IMagneticObjectServices _magneticObjectServices;
    private Rigidbody2D _rigidbody;

    public void Configuration(IMagneticObjectServices magneticObjectServices)
    {
        _magneticObjectServices = magneticObjectServices;
    }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    public void MagnetismOn(Vector3 origenForce, Vector3 endForce)
    {
        _magneticObjectServices.UseMagnetism(origenForce, endForce);
    }

    public void MagnetismOff()
    {
        _magneticObjectServices.StopMagnetism();
    }

    void IMagneticObjectController.UseMagnetism(Vector3 velocity)
    {
        _rigidbody.velocity = velocity;
    }

    void IMagneticObjectController.StopMagnetism(Vector3 velocity)
    {
        _rigidbody.velocity = velocity;
    }
}