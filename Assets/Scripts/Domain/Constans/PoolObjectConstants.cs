﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public abstract class PoolObjectConstants
{
    public const string ENEMY_POOL_TAG = "ENEMYPOLL";
    public const string BULLET_POOL_TAG = "BULLETPOLL";
}