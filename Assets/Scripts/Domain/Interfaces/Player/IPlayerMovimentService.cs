﻿using System;
using UnityEngine;

public interface IPlayerMovimentService
{
    void Move(Vector2 direction, float speed);
}