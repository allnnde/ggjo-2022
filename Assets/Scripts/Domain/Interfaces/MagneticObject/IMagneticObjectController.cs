using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMagneticObjectController
{
    public void UseMagnetism(Vector3 velocity);

    public void StopMagnetism(Vector3 velocity);
}