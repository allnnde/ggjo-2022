﻿using UnityEngine;

public interface IMagneticObjectServices
{
    void StopMagnetism();
    void UseMagnetism(Vector3 origin, Vector3 position);
}