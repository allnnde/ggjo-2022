﻿using System.Collections.Generic;
using UnityEngine;

public class GameInit : MonoBehaviour
{
    public BulletController objPollBullet;

    private void Awake()
    {
        ObjectPool.Instance.Pools = new List<Pool>();
        ObjectPool.Instance.Pools.Add(new Pool() { Item = objPollBullet, Parent = transform, Size = 50, Tag = PoolObjectConstants.BULLET_POOL_TAG });
        ObjectPool.Instance.Start();

        var playerController = FindObjectsOfType(typeof(PlayerController)) as PlayerController[];

        foreach (var item in playerController)
        {
            var _movimentService = new PlayerMovimentService(item);
            item.Configuration(_movimentService);
        }

        var magneticObjectController = FindObjectsOfType(typeof(MagneticObjectController)) as MagneticObjectController[];

        foreach (var item in magneticObjectController)
        {
            var _magneticObjectServices = new MagneticObjectServices(item);
            item.Configuration(_magneticObjectServices);
        }
    }
}