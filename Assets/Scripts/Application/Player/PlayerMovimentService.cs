﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

public class PlayerMovimentService : IPlayerMovimentService
{
    private readonly IPlayerMovimentController _playerMovimentController;

    public PlayerMovimentService(IPlayerMovimentController playerMovimentController)
    {
        _playerMovimentController = playerMovimentController;
    }

    public void Move(Vector2 direction, float speed)
    {
        var res = CalculateVectorMoviment(direction, speed);
        _playerMovimentController.Move(res);
    }

    private Vector2 CalculateVectorMoviment(Vector2 direction, float speed)
    {
        return new Vector2(direction.x * speed, direction.y);
    }
}