using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagneticObjectServices : IMagneticObjectServices
{
    private readonly IMagneticObjectController _interactiveObjectController;

    public MagneticObjectServices(IMagneticObjectController interactiveObjectController)
    {
        _interactiveObjectController = interactiveObjectController;
    }

    public void UseMagnetism(Vector3 originForce, Vector3 endForce)
    {
        var direction = originForce - endForce;

        var dis = Mathf.Abs(Vector2.Distance(originForce, direction));
        if (dis < 1.2)
        {
            StopMagnetism();
        }

        Debug.Log(new Vector2(direction.x, 0).normalized);
        _interactiveObjectController.UseMagnetism(new Vector2(direction.x, 0).normalized);
    }

    public void StopMagnetism()
    {
        _interactiveObjectController.StopMagnetism(new Vector2(0, 0));
    }
}